/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "project.h"
#include "addr_input.h"

uint8 first_digit = 0;
uint8 second_digit = 0;
int switch_digits = 0;

int get_dest_address()
{
    CapSense_InitializeAllBaselines();
    uint16 result = 0;
    
    LCD_ClearDisplay();
    LCD_Position(0,0);
    LCD_PrintString("Destination");
    LCD_Position(1,0);
    LCD_PrintString("Address");
    CyDelay(2000);
    LCD_ClearDisplay();
    LCD_PrintString("Slide to 1st hex");
    LCD_Position(1,0);
    LCD_PrintString("# then press SW3");
    CyDelay(3000);
    LCD_ClearDisplay();
    
    while(switch_digits != 2)
    {
        if(0u == CapSense_IsBusy())
        {
            CapSense_UpdateEnabledBaselines();
            CapSense_ScanEnabledWidgets();
        }  

    
       /* if(CapSense_CheckIsWidgetActive(CapSense_BUTTON0__BTN))
        {
            switch_digits++;
        }*/
        if(select_digit_Read() == 0)
        {
            //button pressed
            switch_digits++;
            CyDelay(500);
            if(switch_digits == 1)
            {
                LCD_Position(0,3);
                LCD_PrintString("Slide to 2nd");
                LCD_Position(1,3);
                LCD_PrintString("#; press SW3");
                CyDelay(2000);
                LCD_ClearDisplay();
            }
        }
        if(CapSense_CheckIsWidgetActive(CapSense_LINEARSLIDER0__LS))
        {
            uint16 position = CapSense_GetCentroidPos(CapSense_LINEARSLIDER0__LS);
            
            if (position == 0xFFFFu)
            {
                //nothing
                //LCD_ClearDisplay();

            }
            else
            {
                LCD_Position(1,0);
                
                if(switch_digits == 0)
                {
                    first_digit = get_digit_2(position);
                    LCD_Position(0,0);
                    LCD_PrintHexUint8(first_digit);
                    
                    //CyDelay(500);
                }
                else if (switch_digits == 1)
                {                  
                    second_digit = get_digit_1(position);
                    LCD_Position(0,0);
                    LCD_PrintHexUint8(first_digit);
                    LCD_Position(1,0);
                    LCD_PrintHexUint8(second_digit);
                    //CyDelay(500);
                }
                /*else if (switch_digits == 2)
                {
                    LCD_ClearDisplay();
                    first_digit = second_digit | first_digit << 4;
                    uint16 temp;
                    uint16 result = 0;
                    for(int i = 0; i < 8; i++)
                    {
                       temp = first_digit & (1 << i);
                       temp = temp >> i;
                       temp = temp << (2*i+1);
                       result = result | temp;
                    }
                    first_digit = result | 0b1000000000000000;
                    
                    switch_digits = 0;
                    
                    LCD_Position(0,0);
                    LCD_PrintHexUint8(first_digit);
                    
                     CapSense_DisableWidget(CapSense_LINEARSLIDER0__LS);
                    CapSense_DisableWidget(CapSense_BUTTON0__BTN);
                    
                }*/
                
                
                
                //LCD_ClearDisplay();
               // LCD_Position(0,0);
               // LCD_PrintHexUint8(first_digit);
                //CyDelay(300);
            }
        }
        else if(switch_digits == 2)
        {
            LCD_ClearDisplay();
            first_digit = second_digit | (first_digit << 4);
            LCD_Position(0,0);
            LCD_PrintHexUint8(first_digit);
            CyDelay(1000);
            
            uint16 temp;
            for(int i = 0; i < 8; i++)
            {
               temp = first_digit & (1 << i);
               temp = temp >> i;
               temp = temp << (2*i+1);
               result = result | temp;
            }
            result = result | 0b1000000000000000;
            //LCD_Position(0,0);
            //LCD_PrintHexUint16(result);
            
            
            
             CapSense_DisableWidget(CapSense_LINEARSLIDER0__LS);
            CapSense_DisableWidget(CapSense_BUTTON0__BTN);
        }
    }
    switch_digits = 0;
    return result;
}

uint8 get_digit_1(int position)
{
    uint8 digit = -1;
    if(position < 6)
    {
        digit = 0;
    }
    else if (position < 11)
    {
       digit = 1;
    }
    else if (position < 17)
    {
        digit = 2;
    }
    else if(position < 23)
    {
        digit = 3;
    }
    else if(position < 29)
    {
        digit = 4;
    }
    else if(position < 35)
    {
        digit = 5;
    }
    else if(position < 41)
    {
        digit = 6;
    }
    else if (position < 47)
    {
       digit = 7;
    }
    else if (position < 53)
    {
        digit = 8;
    }
    else if(position < 59)
    {
        digit = 9;
    }
    else if(position < 65)
    {
        digit = 10;
    }
    else if(position < 71)
    {
        digit = 11;
    }
    else if(position < 78)
    {
        digit = 12;
    }
    else if(position < 85)
    {
        digit = 13;
    }
    else if(position < 92)
    {
        digit = 14;
    }
    else if(position < 99)
    {
        digit = 15;
    }
    
    return digit;
}

uint8 get_digit_2(int position)
{
    uint8 digit = -1;
    if (position < 12)
    {
        digit = 0;
    }
    else if (position < 24)
    {
        digit = 1;
    }
    else if (position < 36)
    {
        digit = 2;
    }
    else if (position < 48)
    {
        digit = 3;
    }
    else if (position < 61)
    {
        digit = 4;
    }
    else if (position < 74)
    {
        digit = 5;
    }
    else if (position < 87)
    {
        digit = 6;
    }
    else if (position < 100)
    {
        digit = 7;
    }
    return digit;
}

/* [] END OF FILE */
