/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef TRANSMIT_H
#define TRANSMIT_H
    
#include "project.h"
    
    int flag;
    
    int isTransmitting;
    
    void uart_init();
    
    void uart_getdata();
    
    void transmit();
    
    void header(uint16 address);
    
    void add_zeros(uint8* buffer_source, uint16* buffer_dest, int count);
    
    int get_dest_address();
#endif

/* [] END OF FILE */
