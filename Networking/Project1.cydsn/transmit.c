/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "transmit.h"
#include "receive.h"
#include "addr_input.h"

#define USBUART_BUFFER_SIZE (64u)
int counter = 0;
//int flag = 0; //o if no ENTER, 1 if ENTER
uint16 double_buffer[USBUART_BUFFER_SIZE*2] = {0};
int have_dest_address = 0;

void uart_init()
{
    USBUART_Start(0, USBUART_3V_OPERATION);
    flag = 0;
    isTransmitting = 0;
}
void add_zeros(uint8* buffer_source, uint16* buffer_dest, int count)
{
    uint16 temp;
    for (int j = 0; j < count; j++) //start at 7th byte, after header
    {
        for(int i = 0; i < 8; i++)
        {
           temp = *buffer_source & (1 << i);
           temp = temp >> i;
           temp = temp << (2*i+1);
           *(buffer_dest) = *(buffer_dest) | temp;
        }
        buffer_source++;
        buffer_dest++;
    }
    buffer_source = buffer_source - count;
    buffer_dest = buffer_dest - (count+7);
}

void uart_getdata()
{
    uint8 buffer[USBUART_BUFFER_SIZE];
    uint8 full_buffer[USBUART_BUFFER_SIZE];
    
    uint16 count;
    flag = 0;
    
    if(0u != USBUART_IsConfigurationChanged())
    {
        if(0u != USBUART_GetConfiguration())
        {
            USBUART_CDC_Init();
        }
    }
    
    if(0u != USBUART_GetConfiguration())
    {
        if (0u != USBUART_DataIsReady())
        {
            
            count = USBUART_GetAll(buffer);
            if(buffer[0] == 0x0D)
            {
                flag = 1;
                
            }
            else
            {
                full_buffer[counter] = buffer[0] + 0b10000000;
                counter = counter+1;
            }
            
            
            
            if(count != 0)
            {
                while(0u == USBUART_CDCIsReady());
                
                USBUART_PutData(buffer, count);
                
                
                if(USBUART_BUFFER_SIZE == count)
                {
                    while(0u == USBUART_CDCIsReady());
                    USBUART_PutData(NULL, 0u);
                }
            }
        }
    }
    
    if (flag == 1)
    {         
        isTransmitting = 1;
        
        /*int addr = -1;
        if(have_dest_address == 0)
        {
            addr = get_dest_address();
        }
        else
        {
            header(addr);
            transmit();
        }*/
        
        int addr = get_dest_address();

        header(addr);
        transmit();

        counter = 0;
        
    }
    else if (flag == 0)
    {
        
        add_zeros(full_buffer,double_buffer+7, counter);
        
    }
       
}

void transmit() 
{
   
    int i = 0;
    uint16 temp;
    while (double_buffer[i] != 0 && flag == 1)
    {       
        for(int j = 15; j >= 0; j--)
        {
            if(flag == 1)
            {          
                //LCD_Position(1,0);
                //LCD_PrintNumber(flag);
                
                temp = double_buffer[i] & (1 << j);
                
                temp = temp >> j;
                //LCD_PrintDecUint16(temp);
                //CyDelay(500);
                transmitted_data_Write(temp);
                
                CyDelayUs(485); //0.5 ms with code delay taken into account
            }
        }
        i++;
    }

    //LCD_Position(0,0);
    //LCD_PrintInt32(flag);
    
    if(flag == 1)
    {
        isTransmitting = 0;
        flag = 0;
        
        //LCD_Position(1,0);
        //LCD_PrintString("test1");
        //clear out buffer
        int i = 0;
        while (double_buffer[i] != 0)
        {
            double_buffer[i] = 0;
            i++;
        }
    }
    
    //stop = 1;
    //flag = 0;
    
}

void header(uint16 address)
{
    //1st bye - 0x80
    double_buffer[0] = 0x8000;
    //2nd byte - 0x81
    double_buffer[1] = 0x8002;
    //3rd byte - source address
    double_buffer[2] = 0x8002; //address 1
    //4th byte - destination address
    double_buffer[3] = address; //broadcast
    //5th byte - logic 1 in MSB + length in binary
    if(counter > 45 || counter < 1)
    {
        LCD_Position(0,0);
        LCD_PrintString("Wrong size! :(");
    }
    else
    {
        uint16 temp;
        uint16 result = 0;
        for(int i = 0; i < 8; i++)
        {
           temp = counter & (1 << i);
           temp = temp >> i;
           temp = temp << (2*i+1);
           result = result | temp;
        }
        double_buffer[4] = result | 0b1000000000000000;
    }  
    //6th byte - 128 (crc not used)
    double_buffer[5] = 0x8000;
    //7th byte - 11110111 (crc not used)
    double_buffer[6] = 0b1010101000101010;  
    
}

/*int get_dest_address()
{
    uint16 count = 0;
    uint8 buffer[USBUART_BUFFER_SIZE];
    uint8 dest_address_buffer[4];
    int counter = 0;
    uint16 address = 0;
    
    if(0u != USBUART_IsConfigurationChanged())
    {
        if(0u != USBUART_GetConfiguration())
        {
            USBUART_CDC_Init();
        }
    }
    
    while(0u == USBUART_CDCIsReady());
    USBUART_PutString("Enter Destination Address in Hex:\n\r0x");
    
    if(0u != USBUART_GetConfiguration())
    {
        if (0u != USBUART_DataIsReady())
        {
            
            count = USBUART_GetAll(buffer);
        }
    }
    if(count != 0)
    {
        
        if(buffer[0] == 0x0D)
        {
            //hit enter
            address = dest_address_buffer[1] | (dest_address_buffer[0] << 4);
            uint16 temp;
            uint16 result = 0;
            for(int i = 0; i < 8; i++)
            {
               temp = address & (1 << i);
               temp = temp >> i;
               temp = temp << (2*i+1);
               result = result | temp;
            }
            address = result | 0b1000000000000000;
            have_dest_address = 1;
        }
        else
        {
            dest_address_buffer[counter] = buffer[0];
            counter++;
        }
        while(0u == USBUART_CDCIsReady());
        //get input
    }
    LCD_Position(0,0);
    LCD_PrintHexUint8(address);
    
    return address;
}*/



/* [] END OF FILE */
