/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "project.h"
#include"receive.h"

#define USBUART_BUFFER_SIZE (64u)

uint8 received_data[USBUART_BUFFER_SIZE*2] = {0};
uint8 bit_buffer[512u] = {0};
int bit_counter = 0;
int msg_length = 0;

void load_up_buffer(uint8 bit)
{
    bit_buffer[bit_counter] = bit;
    bit_counter++;
    
}

void receive()
{
    Receive_TimerISR_Disable();
    Receive_Timer_Stop();
    /*LCD_ClearDisplay();
    for(int k = 0; k < 16; k++)
    {
        LCD_Position(0, k);
        LCD_PrintNumber(bit_buffer[k]);
    }*/    
    
    int i = 0;
    int character = -1;
    int stop = 0;
    
    uint16 temp;// = bit_buffer[0];
    /*LCD_Position(0, 11);
    LCD_PrintNumber(bit_buffer[8]);*/
    
    while(stop == 0)
    {
       temp = bit_buffer[i];
        
        if(i % 8 == 0)
        {
            /*LCD_Position(0, 12);
            LCD_PrintNumber(temp);*/
            //new character
            if (temp != 1)
            {
               //stop receiving
                stop = 1;
            }
            else
            {
                character++;
            }
        }
        
        if(stop == 0)
        {
            //receive this character
            received_data[character] = received_data[character] << 1;
            received_data[character] = received_data[character] | temp;
            i++;
            
        }
    }
    
    bit_counter = 0;
    
    for(int j = 0; j<=character*8; j++)
    {
        bit_buffer[j] = 0;
    }
    //character = -1;
    if(interpret_header(received_data) == 0)
    {
        //LCD_PrintString("here");
        uart_putdata();
    }
    else
    {
        //LCD_PrintString("here");
    }
}
    
void uart_putdata()
{
    //uint8 first_line[16] = {0};
    uint8 third_line[16] = {0};
    
    if(0u != USBUART_IsConfigurationChanged())
    {
        if(0u != USBUART_GetConfiguration())
        {
            USBUART_CDC_Init();
        }
    }

    LCD_ClearDisplay();
    int i = 0;
    //while (received_data[i+7] != 0)
    for(int j=0; j < msg_length; j++)
    {
        if(i < 16)
        {
            LCD_Position(0, i);
            //first_line[i] = received_data[i+7];
            LCD_PutChar(received_data[i+7] & 0b01111111);
        }
        else if (i < 32)
        {
            LCD_Position(1, i-16);
            LCD_PutChar(received_data[i+7] & 0b01111111);
        }
        else
        {
            third_line[i-32] = received_data[i+7];
        }

        i++;
    }
    
    CyDelay(2500);
    LCD_Position(0,0);
    LCD_PrintString("                ");
    int k = 0;
    while(third_line[k] != 0)
    {
        LCD_Position(0,k);
        LCD_PutChar(third_line[k] & 0b01111111);
        k++;
    }
    
    while(0u == USBUART_CDCIsReady());
    USBUART_PutCRLF();
    
    int j = 0;
    while (received_data[j] != 0)
    {
        received_data[j] = 0;
        j++;
    }
    
    receive_bit_counter = 0;
}

int interpret_header()
{
    LCD_Position(0,0);
    //if(received_data[3] > 128 && (received_data[3]& 0b01111111) < 0x7F) // 1 to 126
    {
        //LCD_PrintString("here");
        if(received_data[3] == 0x80 || received_data[3] == 0x83)
        {
            //LCD_PrintString("here");
            //CyDelay(500);
            if(received_data[4] > 128 && received_data[4] <= 172) //0 to 44 with MSB 1
            {
                msg_length = received_data[4] & 0b01111111;
                if(received_data[0] == 0x80 && received_data[1] == 0x81)
                {
                    if(received_data[2] > 128 && received_data[2] < 255)
                    {
                        if(received_data[5] == 128 && received_data[6] == 0b11110111)
                        {
                            return 0;
                        }
                        else
                        {
                            LCD_PrintString("Err: CRC consts");
                            LCD_Position(1,0);
                            LCD_PrintString("Reset system");
                        }
                        
                    }
                    else
                    {
                        LCD_PrintString("Err: Source Addr");
                        LCD_Position(1,0);
                        LCD_PrintString("Reset system");
                    }
                    
                }
                else
                {
                    LCD_PrintString("Err: 1st 2 bytes");
                    LCD_Position(1,0);
                    LCD_PrintString("Reset system");
                }
                
            }
            else
            {
                LCD_PrintString("Err:Wrong Length");
                LCD_Position(1,0);
                LCD_PrintString("Reset system");
            }
        }
        
    }
    return 1;
}

/* [] END OF FILE */
