/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <stdlib.h>
#include "project.h"
#include "transmit.h"
#include "receive.h"

enum state {idle, busy, collision} state;
int temporary = 0;
uint8 this_bit;

CY_ISR(timer_interrupt)
{
    if((int) Wave_in_Read() == 1)
    {
        if (state != collision)
        {
            flag = 2;
        }
        
        state = collision;
        
    }
    else
    {
        state = idle;
    }
    
    
}

CY_ISR(edge_interrupt)
{
    Wave_in_ClearInterrupt();
    if((state == idle || state == busy) && Wave_in_Read() == 1)
    {
        Timer_Stop();
        Timer_WriteCounter(25); //0.52 ms
        Timer_Start();
        state = busy;
    }
    else if((state == busy || state == collision) && Wave_in_Read() == 0)
    {
        Timer_Stop();
        Timer_WriteCounter(404); //8.1 ms
        Timer_Start();
        state = busy;
    }
}

CY_ISR(receive_edge_interrupt)
{
    receive_in_ClearInterrupt();
    Receive_Timer_Stop();
    Receive_Timer_WriteCounter(12); //0.24 ms
    Receive_Timer_Start(); //0.24 ms
    Receive_TimerISR_Enable();   
}

CY_ISR(receive_timer_interrupt)
{

    Receive_Timer_Stop();
    Receive_Timer_WriteCounter(50); //1 ms
    Receive_Timer_Start();

    this_bit = receive_in_Read();
    load_up_buffer(this_bit);
    receive_bit_counter++;
    
    if ((receive_bit_counter-1) % 8 == 0 && this_bit == 0)
    {
        receive();
    }
}

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */
    receive_bit_counter = 0;
    
    LCD_Start();
    uart_init();
    CapSense_Start();
    
    Timer_Start();
    TimerISR_Start();
    TimerISR_StartEx(timer_interrupt);
    EdgeISR_Start();
    EdgeISR_StartEx(edge_interrupt);
    
    Receive_Edge_ISR_Start();
    Receive_Edge_ISR_StartEx(receive_edge_interrupt);
    Receive_TimerISR_Start();
    Receive_TimerISR_StartEx(receive_timer_interrupt);
    
    state = idle;
    
    //get_dest_address();
    LCD_Position(0,0);
    LCD_PrintString("Begin transmit:");
    LCD_Position(1,0);
    LCD_PrintString("Put msg in UART");
    
    
    
    while(1)
    {
        /*if(initiate_transmission_Read() == 0)
        {
            header(get_dest_address());
        }*/
        
        switch(state)
        {
            case idle:
                Idle_LED_Write(1);
                Busy_LED_Write(0);
                Collision_LED_Write(0);
                //CyDelay(500);
                break;
            case busy:
                Idle_LED_Write(0);
                Busy_LED_Write(1);
                Collision_LED_Write(0);
                //CyDelay(500);
                break;
            case collision:
                Idle_LED_Write(0);
                Busy_LED_Write(0);
                Collision_LED_Write(1);
                //CyDelay(500);
                break;
        }
        
        if(state == idle && isTransmitting == 0)
        {
            uart_getdata();
        }
        else if (state == idle && isTransmitting == 1)
        {
            transmit();
        }
        else if(state == collision && isTransmitting == 1)
        {
            
            flag = 1;
            
            int r = rand() % 128;
            r++;
            double time = ((double) r / 128.0)*1000; //delay in ms
            
            
            CyDelay(time);
            
            while(state != idle)
            {
               
            }
            
        }
    }
}

/* [] END OF FILE */
