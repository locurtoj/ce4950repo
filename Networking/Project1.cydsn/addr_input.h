/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef ADDR_INPUT_H
#define ADDR_INPUT_H
    #include "project.h"
    
    int get_dest_address();
    
    uint8 get_digit_1(int position);
    uint8 get_digit_2(int position);
    
#endif


/* [] END OF FILE */
