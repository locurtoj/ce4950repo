/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef RECEIVE_H
#define RECEIVE_H
    
    #include "project.h"
    
    int receive_bit_counter;
    
    void load_up_buffer(uint8 bit);
    
    void receive();
    
    void uart_putdata();
    
    int interpret_header();
#endif

/* [] END OF FILE */
