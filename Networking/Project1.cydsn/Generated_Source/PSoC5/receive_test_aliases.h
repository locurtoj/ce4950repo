/*******************************************************************************
* File Name: receive_test.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_receive_test_ALIASES_H) /* Pins receive_test_ALIASES_H */
#define CY_PINS_receive_test_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"


/***************************************
*              Constants        
***************************************/
#define receive_test_0			(receive_test__0__PC)
#define receive_test_0_INTR	((uint16)((uint16)0x0001u << receive_test__0__SHIFT))

#define receive_test_INTR_ALL	 ((uint16)(receive_test_0_INTR))

#endif /* End Pins receive_test_ALIASES_H */


/* [] END OF FILE */
