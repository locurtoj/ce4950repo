/*******************************************************************************
* File Name: select_digit.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_select_digit_H) /* Pins select_digit_H */
#define CY_PINS_select_digit_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "select_digit_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 select_digit__PORT == 15 && ((select_digit__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    select_digit_Write(uint8 value);
void    select_digit_SetDriveMode(uint8 mode);
uint8   select_digit_ReadDataReg(void);
uint8   select_digit_Read(void);
void    select_digit_SetInterruptMode(uint16 position, uint16 mode);
uint8   select_digit_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the select_digit_SetDriveMode() function.
     *  @{
     */
        #define select_digit_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define select_digit_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define select_digit_DM_RES_UP          PIN_DM_RES_UP
        #define select_digit_DM_RES_DWN         PIN_DM_RES_DWN
        #define select_digit_DM_OD_LO           PIN_DM_OD_LO
        #define select_digit_DM_OD_HI           PIN_DM_OD_HI
        #define select_digit_DM_STRONG          PIN_DM_STRONG
        #define select_digit_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define select_digit_MASK               select_digit__MASK
#define select_digit_SHIFT              select_digit__SHIFT
#define select_digit_WIDTH              1u

/* Interrupt constants */
#if defined(select_digit__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in select_digit_SetInterruptMode() function.
     *  @{
     */
        #define select_digit_INTR_NONE      (uint16)(0x0000u)
        #define select_digit_INTR_RISING    (uint16)(0x0001u)
        #define select_digit_INTR_FALLING   (uint16)(0x0002u)
        #define select_digit_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define select_digit_INTR_MASK      (0x01u) 
#endif /* (select_digit__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define select_digit_PS                     (* (reg8 *) select_digit__PS)
/* Data Register */
#define select_digit_DR                     (* (reg8 *) select_digit__DR)
/* Port Number */
#define select_digit_PRT_NUM                (* (reg8 *) select_digit__PRT) 
/* Connect to Analog Globals */                                                  
#define select_digit_AG                     (* (reg8 *) select_digit__AG)                       
/* Analog MUX bux enable */
#define select_digit_AMUX                   (* (reg8 *) select_digit__AMUX) 
/* Bidirectional Enable */                                                        
#define select_digit_BIE                    (* (reg8 *) select_digit__BIE)
/* Bit-mask for Aliased Register Access */
#define select_digit_BIT_MASK               (* (reg8 *) select_digit__BIT_MASK)
/* Bypass Enable */
#define select_digit_BYP                    (* (reg8 *) select_digit__BYP)
/* Port wide control signals */                                                   
#define select_digit_CTL                    (* (reg8 *) select_digit__CTL)
/* Drive Modes */
#define select_digit_DM0                    (* (reg8 *) select_digit__DM0) 
#define select_digit_DM1                    (* (reg8 *) select_digit__DM1)
#define select_digit_DM2                    (* (reg8 *) select_digit__DM2) 
/* Input Buffer Disable Override */
#define select_digit_INP_DIS                (* (reg8 *) select_digit__INP_DIS)
/* LCD Common or Segment Drive */
#define select_digit_LCD_COM_SEG            (* (reg8 *) select_digit__LCD_COM_SEG)
/* Enable Segment LCD */
#define select_digit_LCD_EN                 (* (reg8 *) select_digit__LCD_EN)
/* Slew Rate Control */
#define select_digit_SLW                    (* (reg8 *) select_digit__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define select_digit_PRTDSI__CAPS_SEL       (* (reg8 *) select_digit__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define select_digit_PRTDSI__DBL_SYNC_IN    (* (reg8 *) select_digit__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define select_digit_PRTDSI__OE_SEL0        (* (reg8 *) select_digit__PRTDSI__OE_SEL0) 
#define select_digit_PRTDSI__OE_SEL1        (* (reg8 *) select_digit__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define select_digit_PRTDSI__OUT_SEL0       (* (reg8 *) select_digit__PRTDSI__OUT_SEL0) 
#define select_digit_PRTDSI__OUT_SEL1       (* (reg8 *) select_digit__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define select_digit_PRTDSI__SYNC_OUT       (* (reg8 *) select_digit__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(select_digit__SIO_CFG)
    #define select_digit_SIO_HYST_EN        (* (reg8 *) select_digit__SIO_HYST_EN)
    #define select_digit_SIO_REG_HIFREQ     (* (reg8 *) select_digit__SIO_REG_HIFREQ)
    #define select_digit_SIO_CFG            (* (reg8 *) select_digit__SIO_CFG)
    #define select_digit_SIO_DIFF           (* (reg8 *) select_digit__SIO_DIFF)
#endif /* (select_digit__SIO_CFG) */

/* Interrupt Registers */
#if defined(select_digit__INTSTAT)
    #define select_digit_INTSTAT            (* (reg8 *) select_digit__INTSTAT)
    #define select_digit_SNAP               (* (reg8 *) select_digit__SNAP)
    
	#define select_digit_0_INTTYPE_REG 		(* (reg8 *) select_digit__0__INTTYPE)
#endif /* (select_digit__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_select_digit_H */


/* [] END OF FILE */
