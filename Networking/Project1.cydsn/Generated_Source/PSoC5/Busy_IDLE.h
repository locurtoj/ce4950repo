/*******************************************************************************
* File Name: Busy_IDLE.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Busy_IDLE_H) /* Pins Busy_IDLE_H */
#define CY_PINS_Busy_IDLE_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "Busy_IDLE_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 Busy_IDLE__PORT == 15 && ((Busy_IDLE__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    Busy_IDLE_Write(uint8 value);
void    Busy_IDLE_SetDriveMode(uint8 mode);
uint8   Busy_IDLE_ReadDataReg(void);
uint8   Busy_IDLE_Read(void);
void    Busy_IDLE_SetInterruptMode(uint16 position, uint16 mode);
uint8   Busy_IDLE_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the Busy_IDLE_SetDriveMode() function.
     *  @{
     */
        #define Busy_IDLE_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define Busy_IDLE_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define Busy_IDLE_DM_RES_UP          PIN_DM_RES_UP
        #define Busy_IDLE_DM_RES_DWN         PIN_DM_RES_DWN
        #define Busy_IDLE_DM_OD_LO           PIN_DM_OD_LO
        #define Busy_IDLE_DM_OD_HI           PIN_DM_OD_HI
        #define Busy_IDLE_DM_STRONG          PIN_DM_STRONG
        #define Busy_IDLE_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define Busy_IDLE_MASK               Busy_IDLE__MASK
#define Busy_IDLE_SHIFT              Busy_IDLE__SHIFT
#define Busy_IDLE_WIDTH              1u

/* Interrupt constants */
#if defined(Busy_IDLE__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in Busy_IDLE_SetInterruptMode() function.
     *  @{
     */
        #define Busy_IDLE_INTR_NONE      (uint16)(0x0000u)
        #define Busy_IDLE_INTR_RISING    (uint16)(0x0001u)
        #define Busy_IDLE_INTR_FALLING   (uint16)(0x0002u)
        #define Busy_IDLE_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define Busy_IDLE_INTR_MASK      (0x01u) 
#endif /* (Busy_IDLE__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define Busy_IDLE_PS                     (* (reg8 *) Busy_IDLE__PS)
/* Data Register */
#define Busy_IDLE_DR                     (* (reg8 *) Busy_IDLE__DR)
/* Port Number */
#define Busy_IDLE_PRT_NUM                (* (reg8 *) Busy_IDLE__PRT) 
/* Connect to Analog Globals */                                                  
#define Busy_IDLE_AG                     (* (reg8 *) Busy_IDLE__AG)                       
/* Analog MUX bux enable */
#define Busy_IDLE_AMUX                   (* (reg8 *) Busy_IDLE__AMUX) 
/* Bidirectional Enable */                                                        
#define Busy_IDLE_BIE                    (* (reg8 *) Busy_IDLE__BIE)
/* Bit-mask for Aliased Register Access */
#define Busy_IDLE_BIT_MASK               (* (reg8 *) Busy_IDLE__BIT_MASK)
/* Bypass Enable */
#define Busy_IDLE_BYP                    (* (reg8 *) Busy_IDLE__BYP)
/* Port wide control signals */                                                   
#define Busy_IDLE_CTL                    (* (reg8 *) Busy_IDLE__CTL)
/* Drive Modes */
#define Busy_IDLE_DM0                    (* (reg8 *) Busy_IDLE__DM0) 
#define Busy_IDLE_DM1                    (* (reg8 *) Busy_IDLE__DM1)
#define Busy_IDLE_DM2                    (* (reg8 *) Busy_IDLE__DM2) 
/* Input Buffer Disable Override */
#define Busy_IDLE_INP_DIS                (* (reg8 *) Busy_IDLE__INP_DIS)
/* LCD Common or Segment Drive */
#define Busy_IDLE_LCD_COM_SEG            (* (reg8 *) Busy_IDLE__LCD_COM_SEG)
/* Enable Segment LCD */
#define Busy_IDLE_LCD_EN                 (* (reg8 *) Busy_IDLE__LCD_EN)
/* Slew Rate Control */
#define Busy_IDLE_SLW                    (* (reg8 *) Busy_IDLE__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define Busy_IDLE_PRTDSI__CAPS_SEL       (* (reg8 *) Busy_IDLE__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define Busy_IDLE_PRTDSI__DBL_SYNC_IN    (* (reg8 *) Busy_IDLE__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define Busy_IDLE_PRTDSI__OE_SEL0        (* (reg8 *) Busy_IDLE__PRTDSI__OE_SEL0) 
#define Busy_IDLE_PRTDSI__OE_SEL1        (* (reg8 *) Busy_IDLE__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define Busy_IDLE_PRTDSI__OUT_SEL0       (* (reg8 *) Busy_IDLE__PRTDSI__OUT_SEL0) 
#define Busy_IDLE_PRTDSI__OUT_SEL1       (* (reg8 *) Busy_IDLE__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define Busy_IDLE_PRTDSI__SYNC_OUT       (* (reg8 *) Busy_IDLE__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(Busy_IDLE__SIO_CFG)
    #define Busy_IDLE_SIO_HYST_EN        (* (reg8 *) Busy_IDLE__SIO_HYST_EN)
    #define Busy_IDLE_SIO_REG_HIFREQ     (* (reg8 *) Busy_IDLE__SIO_REG_HIFREQ)
    #define Busy_IDLE_SIO_CFG            (* (reg8 *) Busy_IDLE__SIO_CFG)
    #define Busy_IDLE_SIO_DIFF           (* (reg8 *) Busy_IDLE__SIO_DIFF)
#endif /* (Busy_IDLE__SIO_CFG) */

/* Interrupt Registers */
#if defined(Busy_IDLE__INTSTAT)
    #define Busy_IDLE_INTSTAT            (* (reg8 *) Busy_IDLE__INTSTAT)
    #define Busy_IDLE_SNAP               (* (reg8 *) Busy_IDLE__SNAP)
    
	#define Busy_IDLE_0_INTTYPE_REG 		(* (reg8 *) Busy_IDLE__0__INTTYPE)
#endif /* (Busy_IDLE__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_Busy_IDLE_H */


/* [] END OF FILE */
