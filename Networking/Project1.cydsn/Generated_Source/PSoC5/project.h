/*******************************************************************************
* File Name: project.h
* 
* PSoC Creator  4.1 Update 1
*
* Description:
* It contains references to all generated header files and should not be modified.
* This file is automatically generated by PSoC Creator.
*
********************************************************************************
* Copyright (c) 2007-2017 Cypress Semiconductor.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "cyfitter_cfg.h"
#include "cydevice.h"
#include "cydevice_trm.h"
#include "cyfitter.h"
#include "cydisabledsheets.h"
#include "Wave_in_aliases.h"
#include "Wave_in.h"
#include "timer_clock.h"
#include "Timer.h"
#include "TimerISR.h"
#include "EdgeISR.h"
#include "Idle_LED_aliases.h"
#include "Idle_LED.h"
#include "Busy_LED_aliases.h"
#include "Busy_LED.h"
#include "Collision_LED_aliases.h"
#include "Collision_LED.h"
#include "LCD.h"
#include "USBUART.h"
#include "USBUART_audio.h"
#include "USBUART_cdc.h"
#include "USBUART_hid.h"
#include "USBUART_midi.h"
#include "USBUART_pvt.h"
#include "USBUART_cydmac.h"
#include "USBUART_msc.h"
#include "transmitted_data_aliases.h"
#include "transmitted_data.h"
#include "Receive_TimerISR.h"
#include "receive_in_aliases.h"
#include "receive_in.h"
#include "Receive_Edge_ISR.h"
#include "timer_clock_1.h"
#include "Receive_Timer.h"
#include "CapSense.h"
#include "CapSense_CSHL.h"
#include "CapSense_Pins.h"
#include "CapSense_TunerHelper.h"
#include "select_digit_aliases.h"
#include "select_digit.h"
#include "initiate_transmission_aliases.h"
#include "initiate_transmission.h"
#include "LCD_LCDPort_aliases.h"
#include "LCD_LCDPort.h"
#include "USBUART_Dm_aliases.h"
#include "USBUART_Dm.h"
#include "USBUART_Dp_aliases.h"
#include "USBUART_Dp.h"
#include "CapSense_CompCH0.h"
#include "CapSense_IdacCH0.h"
#include "CapSense_AMuxCH0.h"
#include "CapSense_IntClock.h"
#include "core_cm3_psoc5.h"
#include "CyDmac.h"
#include "CyFlash.h"
#include "CyLib.h"
#include "cypins.h"
#include "cyPm.h"
#include "CySpc.h"
#include "cytypes.h"

/*[]*/

