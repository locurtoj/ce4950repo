/*******************************************************************************
* File Name: compare_val.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_compare_val_H) /* Pins compare_val_H */
#define CY_PINS_compare_val_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "compare_val_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 compare_val__PORT == 15 && ((compare_val__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    compare_val_Write(uint8 value);
void    compare_val_SetDriveMode(uint8 mode);
uint8   compare_val_ReadDataReg(void);
uint8   compare_val_Read(void);
void    compare_val_SetInterruptMode(uint16 position, uint16 mode);
uint8   compare_val_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the compare_val_SetDriveMode() function.
     *  @{
     */
        #define compare_val_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define compare_val_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define compare_val_DM_RES_UP          PIN_DM_RES_UP
        #define compare_val_DM_RES_DWN         PIN_DM_RES_DWN
        #define compare_val_DM_OD_LO           PIN_DM_OD_LO
        #define compare_val_DM_OD_HI           PIN_DM_OD_HI
        #define compare_val_DM_STRONG          PIN_DM_STRONG
        #define compare_val_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define compare_val_MASK               compare_val__MASK
#define compare_val_SHIFT              compare_val__SHIFT
#define compare_val_WIDTH              1u

/* Interrupt constants */
#if defined(compare_val__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in compare_val_SetInterruptMode() function.
     *  @{
     */
        #define compare_val_INTR_NONE      (uint16)(0x0000u)
        #define compare_val_INTR_RISING    (uint16)(0x0001u)
        #define compare_val_INTR_FALLING   (uint16)(0x0002u)
        #define compare_val_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define compare_val_INTR_MASK      (0x01u) 
#endif /* (compare_val__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define compare_val_PS                     (* (reg8 *) compare_val__PS)
/* Data Register */
#define compare_val_DR                     (* (reg8 *) compare_val__DR)
/* Port Number */
#define compare_val_PRT_NUM                (* (reg8 *) compare_val__PRT) 
/* Connect to Analog Globals */                                                  
#define compare_val_AG                     (* (reg8 *) compare_val__AG)                       
/* Analog MUX bux enable */
#define compare_val_AMUX                   (* (reg8 *) compare_val__AMUX) 
/* Bidirectional Enable */                                                        
#define compare_val_BIE                    (* (reg8 *) compare_val__BIE)
/* Bit-mask for Aliased Register Access */
#define compare_val_BIT_MASK               (* (reg8 *) compare_val__BIT_MASK)
/* Bypass Enable */
#define compare_val_BYP                    (* (reg8 *) compare_val__BYP)
/* Port wide control signals */                                                   
#define compare_val_CTL                    (* (reg8 *) compare_val__CTL)
/* Drive Modes */
#define compare_val_DM0                    (* (reg8 *) compare_val__DM0) 
#define compare_val_DM1                    (* (reg8 *) compare_val__DM1)
#define compare_val_DM2                    (* (reg8 *) compare_val__DM2) 
/* Input Buffer Disable Override */
#define compare_val_INP_DIS                (* (reg8 *) compare_val__INP_DIS)
/* LCD Common or Segment Drive */
#define compare_val_LCD_COM_SEG            (* (reg8 *) compare_val__LCD_COM_SEG)
/* Enable Segment LCD */
#define compare_val_LCD_EN                 (* (reg8 *) compare_val__LCD_EN)
/* Slew Rate Control */
#define compare_val_SLW                    (* (reg8 *) compare_val__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define compare_val_PRTDSI__CAPS_SEL       (* (reg8 *) compare_val__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define compare_val_PRTDSI__DBL_SYNC_IN    (* (reg8 *) compare_val__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define compare_val_PRTDSI__OE_SEL0        (* (reg8 *) compare_val__PRTDSI__OE_SEL0) 
#define compare_val_PRTDSI__OE_SEL1        (* (reg8 *) compare_val__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define compare_val_PRTDSI__OUT_SEL0       (* (reg8 *) compare_val__PRTDSI__OUT_SEL0) 
#define compare_val_PRTDSI__OUT_SEL1       (* (reg8 *) compare_val__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define compare_val_PRTDSI__SYNC_OUT       (* (reg8 *) compare_val__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(compare_val__SIO_CFG)
    #define compare_val_SIO_HYST_EN        (* (reg8 *) compare_val__SIO_HYST_EN)
    #define compare_val_SIO_REG_HIFREQ     (* (reg8 *) compare_val__SIO_REG_HIFREQ)
    #define compare_val_SIO_CFG            (* (reg8 *) compare_val__SIO_CFG)
    #define compare_val_SIO_DIFF           (* (reg8 *) compare_val__SIO_DIFF)
#endif /* (compare_val__SIO_CFG) */

/* Interrupt Registers */
#if defined(compare_val__INTSTAT)
    #define compare_val_INTSTAT            (* (reg8 *) compare_val__INTSTAT)
    #define compare_val_SNAP               (* (reg8 *) compare_val__SNAP)
    
	#define compare_val_0_INTTYPE_REG 		(* (reg8 *) compare_val__0__INTTYPE)
#endif /* (compare_val__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_compare_val_H */


/* [] END OF FILE */
