/*******************************************************************************
* File Name: select_digit.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_select_digit_ALIASES_H) /* Pins select_digit_ALIASES_H */
#define CY_PINS_select_digit_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"


/***************************************
*              Constants        
***************************************/
#define select_digit_0			(select_digit__0__PC)
#define select_digit_0_INTR	((uint16)((uint16)0x0001u << select_digit__0__SHIFT))

#define select_digit_INTR_ALL	 ((uint16)(select_digit_0_INTR))

#endif /* End Pins select_digit_ALIASES_H */


/* [] END OF FILE */
