/*******************************************************************************
* File Name: initiate_transmission.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_initiate_transmission_H) /* Pins initiate_transmission_H */
#define CY_PINS_initiate_transmission_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "initiate_transmission_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 initiate_transmission__PORT == 15 && ((initiate_transmission__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    initiate_transmission_Write(uint8 value);
void    initiate_transmission_SetDriveMode(uint8 mode);
uint8   initiate_transmission_ReadDataReg(void);
uint8   initiate_transmission_Read(void);
void    initiate_transmission_SetInterruptMode(uint16 position, uint16 mode);
uint8   initiate_transmission_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the initiate_transmission_SetDriveMode() function.
     *  @{
     */
        #define initiate_transmission_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define initiate_transmission_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define initiate_transmission_DM_RES_UP          PIN_DM_RES_UP
        #define initiate_transmission_DM_RES_DWN         PIN_DM_RES_DWN
        #define initiate_transmission_DM_OD_LO           PIN_DM_OD_LO
        #define initiate_transmission_DM_OD_HI           PIN_DM_OD_HI
        #define initiate_transmission_DM_STRONG          PIN_DM_STRONG
        #define initiate_transmission_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define initiate_transmission_MASK               initiate_transmission__MASK
#define initiate_transmission_SHIFT              initiate_transmission__SHIFT
#define initiate_transmission_WIDTH              1u

/* Interrupt constants */
#if defined(initiate_transmission__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in initiate_transmission_SetInterruptMode() function.
     *  @{
     */
        #define initiate_transmission_INTR_NONE      (uint16)(0x0000u)
        #define initiate_transmission_INTR_RISING    (uint16)(0x0001u)
        #define initiate_transmission_INTR_FALLING   (uint16)(0x0002u)
        #define initiate_transmission_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define initiate_transmission_INTR_MASK      (0x01u) 
#endif /* (initiate_transmission__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define initiate_transmission_PS                     (* (reg8 *) initiate_transmission__PS)
/* Data Register */
#define initiate_transmission_DR                     (* (reg8 *) initiate_transmission__DR)
/* Port Number */
#define initiate_transmission_PRT_NUM                (* (reg8 *) initiate_transmission__PRT) 
/* Connect to Analog Globals */                                                  
#define initiate_transmission_AG                     (* (reg8 *) initiate_transmission__AG)                       
/* Analog MUX bux enable */
#define initiate_transmission_AMUX                   (* (reg8 *) initiate_transmission__AMUX) 
/* Bidirectional Enable */                                                        
#define initiate_transmission_BIE                    (* (reg8 *) initiate_transmission__BIE)
/* Bit-mask for Aliased Register Access */
#define initiate_transmission_BIT_MASK               (* (reg8 *) initiate_transmission__BIT_MASK)
/* Bypass Enable */
#define initiate_transmission_BYP                    (* (reg8 *) initiate_transmission__BYP)
/* Port wide control signals */                                                   
#define initiate_transmission_CTL                    (* (reg8 *) initiate_transmission__CTL)
/* Drive Modes */
#define initiate_transmission_DM0                    (* (reg8 *) initiate_transmission__DM0) 
#define initiate_transmission_DM1                    (* (reg8 *) initiate_transmission__DM1)
#define initiate_transmission_DM2                    (* (reg8 *) initiate_transmission__DM2) 
/* Input Buffer Disable Override */
#define initiate_transmission_INP_DIS                (* (reg8 *) initiate_transmission__INP_DIS)
/* LCD Common or Segment Drive */
#define initiate_transmission_LCD_COM_SEG            (* (reg8 *) initiate_transmission__LCD_COM_SEG)
/* Enable Segment LCD */
#define initiate_transmission_LCD_EN                 (* (reg8 *) initiate_transmission__LCD_EN)
/* Slew Rate Control */
#define initiate_transmission_SLW                    (* (reg8 *) initiate_transmission__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define initiate_transmission_PRTDSI__CAPS_SEL       (* (reg8 *) initiate_transmission__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define initiate_transmission_PRTDSI__DBL_SYNC_IN    (* (reg8 *) initiate_transmission__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define initiate_transmission_PRTDSI__OE_SEL0        (* (reg8 *) initiate_transmission__PRTDSI__OE_SEL0) 
#define initiate_transmission_PRTDSI__OE_SEL1        (* (reg8 *) initiate_transmission__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define initiate_transmission_PRTDSI__OUT_SEL0       (* (reg8 *) initiate_transmission__PRTDSI__OUT_SEL0) 
#define initiate_transmission_PRTDSI__OUT_SEL1       (* (reg8 *) initiate_transmission__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define initiate_transmission_PRTDSI__SYNC_OUT       (* (reg8 *) initiate_transmission__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(initiate_transmission__SIO_CFG)
    #define initiate_transmission_SIO_HYST_EN        (* (reg8 *) initiate_transmission__SIO_HYST_EN)
    #define initiate_transmission_SIO_REG_HIFREQ     (* (reg8 *) initiate_transmission__SIO_REG_HIFREQ)
    #define initiate_transmission_SIO_CFG            (* (reg8 *) initiate_transmission__SIO_CFG)
    #define initiate_transmission_SIO_DIFF           (* (reg8 *) initiate_transmission__SIO_DIFF)
#endif /* (initiate_transmission__SIO_CFG) */

/* Interrupt Registers */
#if defined(initiate_transmission__INTSTAT)
    #define initiate_transmission_INTSTAT            (* (reg8 *) initiate_transmission__INTSTAT)
    #define initiate_transmission_SNAP               (* (reg8 *) initiate_transmission__SNAP)
    
	#define initiate_transmission_0_INTTYPE_REG 		(* (reg8 *) initiate_transmission__0__INTTYPE)
#endif /* (initiate_transmission__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_initiate_transmission_H */


/* [] END OF FILE */
